/* ----------------------------------------------------------------------
   HEIG-VD Yverdon-les-Bains
   ------------------------------------------------------------------------

   Auteur : Pierrick MULLER
   Email  : pierrick.muller@heig-vd.ch
   Date   : 12/03/2019
   Modified: -
   Description : gpio_toolbox.c
   -----------------------------------------------------------------------
*/

#include "stddefs.h"
#include "bits.h"
#include "gpio.h"

uchar GeneralPurposeOutput(uchar module, ulong bitmask, ulong offset,uchar mods);

//Fonction permettant de mettre une sortie � 1
void SetOutput(uchar module, ulong bitmask)
{
	GeneralPurposeOutput(module,bitmask,OMAP_GPIO_SETDATAOUT,1);
}

//Fonction permettant de mettre une sortie � 0
void ClearOutput(uchar module, ulong bitmask)
{
	GeneralPurposeOutput(module,bitmask,OMAP_GPIO_CLEARDATAOUT,1);
}

//Fonction permettant de connaitre l'�tat d'une entr�e
uchar ReadInput(uchar module, ulong bitmask)
{
	return GeneralPurposeOutput(module,bitmask,OMAP_GPIO_DATAIN,2);
}

//Fonction permettant d'inverser l'�tat d'une sortie (1->0,0->1)
void ToggleOutput(uchar module, ulong bitmask)
{
	GeneralPurposeOutput(module,bitmask,OMAP_GPIO_DATAOUT,0);
}

void UnmaskIRQ(uchar module, ulong bitmask)
{

}

void MaskIRQ(uchar module, ulong bitmask)
{

}

//Fonction permettant de g�rer l'utilisation des GPIO � l'aide d'un switch
//Le mods sert a savoir quelle action effectuer.Pour lire une entr�e c'est 2, pour effectuer un OR c'est un 1 et 0 pour un xor
uchar GeneralPurposeOutput(uchar module, ulong bitmask, ulong offset,uchar mods)
{
	//On effectue les operations souhait�es en fonction du module et du mods.
	switch(module)
		{
		case 1:
			if(mods == 1)
			{
				GPIO1_REG(offset) = bitmask;
			}
			else if(mods == 0)
			{
				GPIO1_REG(offset) ^= bitmask;
			}
			else if(mods == 2)
			{
				return (uchar)((GPIO1_REG(offset) & bitmask) >> 8);
			}
			break;
		case 2:
			if(mods == 1)
			{
				GPIO2_REG(offset) = bitmask;
			}
			else if(mods == 0)
			{
				GPIO2_REG(offset) ^= bitmask;
			}
			else if(mods == 2)
			{
				return (uchar)((GPIO2_REG(offset) & bitmask) >> 8);
			}
			break;
		case 3:
			if(mods == 1)
			{
				GPIO3_REG(offset) = bitmask;
			}
			else if(mods == 0)
			{
				GPIO3_REG(offset) ^= bitmask;
			}
			else if(mods == 2)
			{
				return (uchar)((GPIO3_REG(offset) & bitmask) >> 8);
			}
			break;
		case 4:
			if(mods == 1)
			{
				GPIO4_REG(offset) = bitmask;
			}
			else if(mods == 0)
			{
				GPIO4_REG(offset) ^= bitmask;
			}
			else if(mods == 2)
			{
				return (uchar)((GPIO4_REG(offset) & bitmask) >> 8);
			}
			break;
		case 5:
			if(mods == 1)
			{
				GPIO5_REG(offset) = bitmask;
			}
			else if(mods == 0)
			{
				GPIO5_REG(offset) ^= bitmask;
			}
			else if(mods == 2)
			{
				return (uchar)((GPIO5_REG(offset) & bitmask) >> 8);
			}
			break;
		case 6:
			if(mods == 1)
			{
				GPIO6_REG(offset) = bitmask;
			}
			else if(mods == 0)
			{
				GPIO6_REG(offset) ^= bitmask;
			}
			else if(mods == 2)
			{
				return (uchar)((GPIO6_REG(offset) & bitmask) >> 8);
			}
			break;
		}
	return 0;
}

