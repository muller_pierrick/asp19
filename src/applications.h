/*
 * applications.h
 *
 *  Created on: Mar 28, 2019
 *  Author: Pierrick Muller
 */

#ifndef APPLICATIONS_H_
#define APPLICATIONS_H_


//Variable globale utile pour l'application
extern uint valueTimer;
uint score;
uint bestScore;
extern uchar running;

extern void launchApp();
void start();
void printMenu();
void checkBestScore();


#endif /* APPLICATIONS_H_ */
