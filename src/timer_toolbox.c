/* ----------------------------------------------------------------------
   HEIG-VD Yverdon-les-Bains
   ------------------------------------------------------------------------

   Auteur : Pierrick MULLER
   Email  : pierrick.muller@heig-vd.ch
   Date   : 21/03/2019
   Description : TIMER toolbox
   -----------------------------------------------------------------------
*/

#include "stddefs.h"
#include "timer.h"
#include "gpio.h"
#include "prcm.h"
#include "bits.h"

uint valTimer = 0;

//Permet de d�marrer le GPTIMER1
void start_timer()
{
	//On met � 1 le bit permettant de d�marrer ou stopper le timer dans le registre TCLR
	//(1 start, 0 stop)
	GPT1_REG(TCLR) |= BIT0;
}

//Permet de stopper le GPTIMER1
void stop_timer()
{	//On met � 0 le bit permettant de d�marrer ou stopper le timer dans le registre TCLR
	//(1 start, 0 stop)
	GPT1_REG(TCLR) &= ~BIT0;
}

//Permet de lire la valeur du GPTIMER1
uint read_timer_value()
{
	//On retourne la valeur du registre TCRR qui contient la valeur du timer (temps d'attente, doit �tre diviser par 32 pour
	//obtenir le nombre de secondes)
	return GPT1_REG(TCRR);
}

//Permet d'ecrire uen valeur dans le GPTIMER1
void write_timer_value(uint val)
{
	//Permet d'ecrire dans le registre TCRR afin de modifier la valeur du timer.
	GPT1_REG(TCRR) = val;
}

//Fonction permettant de patienter pendant un certain nombre de seconde
void sleep(uint sec)
{
	start_timer();
	while(read_timer_value() < (sec*32*1000));
	stop_timer();
	write_timer_value(0);
}
