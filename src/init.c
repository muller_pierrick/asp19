/*
 * init.c
 *
 *  Created on: 21 mars 2012
 *      Author: Evangelina Lolivier-Exler
 *      Basic functions of initialization of the REPTAR board
 */

#include "bits.h"
#include "stddefs.h"
#include "gpio.h"
#include "padconf.h"
#include "lcd.h"
#include "prcm.h"
#include "gpio_toolbox.h"
#include "timer_toolbox.h"
#include "lcd_toolbox.h"
#include "bits.h"
#include "intc.h"
#include "timer.h"
#include "applications.h"
#include <stdio.h>

vulong PadConfVal=0;
vulong PadConfValLED=0;         // GPIO_141 & GPIO_143
vulong PadConfValSW=0;          // GPIO_140 & GPIO_142

extern unsigned long isr(void);       //link to isr in file int_arm.S

void GPIO_init(){
    // initialization sequence: software reset
    GPIO5_REG(SYSCONFIG) |= BIT1;
    while ((GPIO5_REG(SYSSTATUS)&BIT0)==0);

    // completer le code...........

    //LED Enable
    //En mettant � 0 les bits correspondants aux leds 0 et 1 de la GPIO 5
    //dans le registre GPIO_OE, on les d�finit comme sorties
    GPIO5_REG(OMAP_GPIO_OE) &= !BIT15;
    GPIO5_REG(OMAP_GPIO_OE) &= !BIT13;

    //En mettant � 1 les bits correspondants aux boutons SW 0 et 1 de la GPIO 5
    //dans le registre GPIO_OE, on les d�finit comme entr�es.
    GPIO5_REG(OMAP_GPIO_OE) |= BIT12;
    GPIO5_REG(OMAP_GPIO_OE) |= BIT14;

    //SW Conf


    /* System Control Module: PAD Configuration */
    //LED
    PadConfValLED=
    	MXMode4 // On definit le mode (MXMode) des leds � 4 en se referrant a la page 2453 de la doc reptar
        |PULLUDENABLE_OFF
        |PULLTYPESELECT_UP
        |INPUTENABLE_OFF
        |OFFENABLE_ON
        |OFFOUTENABLE_OUT
        |OFFOUTVALUE_HI
        |OFFPULLUDENABLE_OFF
        |OFFPULLTYPESELECT_UP
        |WAKEUPENABLE_OFF;


    //SW
    PadConfValSW=
    	MXMode4 // On definit le mode (MXMode) des leds � 4 en se referrant a la page 2453 de la doc reptar
        |PULLUDENABLE_ON
        |PULLTYPESELECT_DWN
        |INPUTENABLE_ON
        |OFFENABLE_ON
        |OFFOUTENABLE_IN
        |OFFOUTVALUE_HI
        |OFFPULLUDENABLE_ON
        |OFFPULLTYPESELECT_DWN
        |WAKEUPENABLE_OFF;

    //On met � jour les registre PADCONF (page 2453 de la doc reptar pour trouver les adresses)
    SET_REG32(CONTROL_PADCONF_MCBSP3_DX) = PadConfValSW | (PadConfValLED<<16);
    SET_REG32(CONTROL_PADCONF_MCBSP3_CLKX) = PadConfValSW | (PadConfValLED<<16);
}



void lcd_init(){

    /* Clock configuration */
    // select DSS1_ALWON_FCLK frequency: DPLL4 divided by 2
    DSS_CM_REG(CM_CLKSEL)&=~DDS1_FIELD_MSK;
    DSS_CM_REG(CM_CLKSEL)|=PER_M4X2;
    // enable functional clock
    DSS_CM_REG(CM_FCLKEN)|= BIT0;
    // enable interfaces clocks (L3 and L4)  (must always be enable for access to the config registers)
    DSS_CM_REG(CM_ICLKEN)|= BIT0;



    /* System Control Module: PAD Configuration */

    // OUTPUT CONFIGURATION
    PadConfVal=0;

    PadConfVal  = MXMode0 |PULLUDENABLE_OFF
        |PULLTYPESELECT_UP
        |INPUTENABLE_OFF
        |OFFENABLE_ON
        |OFFOUTENABLE_OUT
        |OFFOUTVALUE_LOW
        |OFFPULLUDENABLE_OFF
        |OFFPULLTYPESELECT_UP
        |WAKEUPENABLE_OFF;

    /* DSS_PCLK (15:0) and HSYNC (31:16) */
    SET_REG32(CONTROL_PADCONF_DSS_PCLK)=PadConfVal|(PadConfVal<<16);
    /* VSYNC (15:0) and ACBIAS (31:16) */
    SET_REG32(CONTROL_PADCONF_DSS_VSYNC)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA0 (15:0) and DSS_DATA1 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA0)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA2 (15:0) and DSS_DATA3 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA2)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA4 (15:0) and DSS_DATA5 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA4)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA6 (15:0) and DSS_DATA7 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA6)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA8 (15:0) and DSS_DATA9 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA8)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA10 (15:0) and DSS_DATA11 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA10)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA12 (15:0) and DSS_DATA13 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA12)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA14 (15:0) and DSS_DATA15 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA14)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA16 (15:0) and DSS_DATA17 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA16)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA18 (15:0) and DSS_DATA19 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA18)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA20 (15:0) and DSS_DATA21 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA20)=PadConfVal|(PadConfVal<<16);
    /* DSS_DATA22 (15:0) and DSS_DATA23 (31:16) */
    SET_REG32(CONTROL_PADCONF_DATA22)=PadConfVal|(PadConfVal<<16);

    /* Display Subsystem (DSS) Configuration */
    DSS_REG(SYSCONFIG)|=BIT1;   // software reset
    while ((DSS_REG(SYSSTATUS)&BIT0)==0); // WAIT FOR RESET DONE

    // capacitive display parameters
    LCD_REG(timing_h)=0x0090097F;                      /* Horizontal timing */
    LCD_REG(timing_v)=0x00B00401;                      /* Vertical timing */
    LCD_REG(pol_freq)=0x00003000;                      /* Pol Freq */

    LCD_REG(divisor)=0x00010002;                       /* 33Mhz Pixel Clock */
    LCD_REG(size_lcd)=0x01df031f;                      /* 800x480 */
    LCD_REG(control)|= (0x01<<TFTSTN_SHIFT);           /* panel_type: TFT */
    LCD_REG(control)|= (0x03<<DATALINES_SHIFT);        /* data_lines: 24 Bit RGB */
    /* GPOUT0 AND GPOUT1: bypass mode */
    LCD_REG(control)|= BIT16;                          /* GPOUT1 */
    LCD_REG(control)|= BIT15;                          /* GPOUT0 */
    LCD_REG(config)= (0x02<<FRAME_MODE_SHIFT);         /* load_mode: Frame Mode */
    LCD_REG(default_color0)=SPLASH_SOLID_COLOR;        /* ORANGE */
    LCD_REG(gfx_attributes)|= (0x6<<GFX_FORMAT_SHIFT); /* GRAPHICS FORMAT: RGB16 */
    LCD_REG(gfx_preload)=0x60;                         /* Preload for TFT display */
    LCD_REG(gfx_ba0)=FRAME_BUFFER_ADDR;                /* Graphics base address */
    LCD_REG(gfx_size)|= (479<<GFX_SIZEY_SHIFT);        /* number of lines of the graphics window */
    LCD_REG(gfx_size)|= 799;                           /* number of pixels per line of the graphics window */
    LCD_REG(gfx_attributes)|= BIT0;                    /* graphics layer enable */
    LCD_REG(control)|= BIT5;                           /* go LCD -> the HW can update the internal registers */

}

//--------------------------------------------------------------------------
// interrupt_init
//
// This function initializes interrupt controller (INTC) and the GPIO interrupts
//
void interrupt_init(){
    /* Pointer to the RAM IRQ vector */
    vulong *IRQ_ram_vector_ptr1;
    /* Pointer to the ISR */
    vulong *IRQ_ram_vector_ptr2;


    // software reset
    //PAge 2423, pour le software reset
    //On effectue le reset software en mettant le bit 1 du registre INTC_SYSCONFIG � 1. Il est remis � 0 automatiquement une fois le reset effectu�
    MPU_INTC_REG(INTC_SYSCONFIG) |= BIT1;
    //On attend la fin du software reset
    while ((MPU_INTC_REG(INTC_SYSSTATUS)&BIT0)==0);

    /* Interrupt Vector Initialization */
    // Set Exception vectors table at address 0x8ff64000
    asm("ldr r0, =0x8FF64000");
    asm("mcr p15, 0, r0, c12, c0, 0");

    IRQ_ram_vector_ptr1=(vulong *)0x8FF64018;   //IRQ vector address: 0x8ff64000 + 18 (Vectors table + offset)  (see page 3555 of the DM37 TRM)
    *IRQ_ram_vector_ptr1=0xE59FF018;            // write the instruction asm("ldr pc,[pc,#0x20]") at the IRQ vector address
    // asm("ldr pc,[pc,#0x20]") => pc=[0x8FF64038]

    IRQ_ram_vector_ptr2=(vulong *)0x8FF64038;   // IRQ vector address (see page 3557 of the DM37 TRM)

    // when an IRQ arrives, the PC loads the content of the address pointed by IRQ_ram_vector_ptr2
    *IRQ_ram_vector_ptr2=(unsigned long) &isr;        // jump to the ISR when an IRQ arrives



    /*  GPIO IRQ Configuration */

    //On active les interuptions pour le button sw0 en mettant le bit correspondant � 1 dans le registre GPIO_SETIRQENABLE1.
    //Cela permet de mettre le bit correspondant dans le registre GPIO_IRQENABLE1 � 1.
    GPIO5_REG(OMAP_GPIO_SETIRQENABLE1) = BIT12;
    //On pr�cise qu'on veut que l'interruption s'active lorsque l'�tat de l'entr�e passe de 0 � 1 (Appui)
    GPIO5_REG(OMAP_GPIO_RISINGDETECT) |= BIT12;

    /*  IRQ Configuration */

    //M_IRQ_33 ,GPIO5_MPU_IRQ ,GPIO module 5 ,page 2408
    //On set la priorit� de notre interuption � 0 (Normalement non n�c�ssaire apr�s le software reset, toutes les priorit�es �tant reset � 0)
    MPU_INTC_REG(MPU_INTC_ILRm(33)) &= 0x0;
    //On demasque l'interruption (33 correspondant aux interruptions de la GPIO5)(Voir page 2408 doc reptar)
    //On demasque le bit 1 car on est sur le second registre (MIR1) et donc 32 + 1 = 33 (Le premier registre contenant 0 � 31)
    MPU_INTC_REG(INTC_MIR1) &= ~BIT1;
    //On reset les hypotetiques interruption et on permet la g�n�ration de nouvelles interruptions
    MPU_INTC_REG(INTC_CONTROL) |= BIT0;

    //Morceau de code fournit par les slides vu en cours.
    asm("MRS r1, cpsr");
    asm("BIC  r1, r1, #0x80");
    asm("MSR cpsr_c, r1");

}

void isr_handler()
{

	//Recup�ration du num�ro de l'interuption
	int interrupt = MPU_INTC_REG(INTC_SIR_IRQ) & 0x7f;

	//--TESTS TIMER PARTIE 4--//
	//uchar snum[80];		  //
	//------------------------//

	//On check si l'interruption vient de la GPIO5
	if(interrupt == 33)
	{
		//--GESTION LED PARTIE 3 --//
		//ToggleOutput(5,BIT15);   //
		//-------------------------//

		//--TESTS TIMER PARTIE 4--//
		/*
		if(read_timer_value() == 0)
		{
			start_timer();
		}
		else
		{
			stop_timer();
			sprintf(snum,"ms : %d",(read_timer_value()/32));
			clear_screen();
			fb_print_string(snum,200,200,14);
			write_timer_value(0);
		}
		*/
		//-------------------------//

		//GESTION INTERRUPTION PARTIE 5--//
		//Gestion de l'appui sur SW0 par interruption (On check que c'est bien le bouton SW0 qui � �t� pes�)
		if(ReadInput(5,BIT12) == (BIT12 >> 8))
		{
			//On controlle que l'on est bien dans le jeu et que l'on attend bien un appui de SW0 � ce moment
			if(running == 1)
			{
				//On r�cup�re la valeur du timer et on reinitialise sa valeur
				stop_timer();
				valueTimer = read_timer_value();
				write_timer_value(0);
			}

		}
	}


	//On acquite l'interuption du cot� de la GPIO
	GPIO5_REG(OMAP_GPIO_IRQSTATUS1) |= BIT12;

	//On dis qu'on peut generer de nouvelles interruptions
	MPU_INTC_REG(INTC_CONTROL) |= BIT0;
}

void timer_init()
{
	//software reset
	GPT1_REG(TIOCP_CFG) |= BIT1;
	while ((GPT1_REG(TISTAT)&BIT0)==0);

	//CONFIGURATION HORLOGE SOURCE
	//On active l'horloge fonctionnelle du GPTIMER1
	WKUP_CM_REG(CM_FCLKEN) |= BIT0;
	//On active l'horloge d'interface du GPTIMER1
	WKUP_CM_REG(CM_ICLKEN) |= BIT0;
	//On choisit l'horloge 32k pour le GPTIMER1
	WKUP_CM_REG(CM_CLKSEL) &= ~BIT0;
}
