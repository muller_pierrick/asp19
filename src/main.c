/*---------------------------------------------------------------------------------------
 * But               : Petit programme pour etre charge dans le micro et affiche avec dump
 * Auteur            : Evangelina LOLIVIER-EXLER
 * Date              : 29.07.2008
 * Version           : 1.0
 * Fichier           : demo.c
 *----------------------------------------------------------------------------------------*/
#include "cfg.h"
#include "stddefs.h"
#include "init.h"
#include "lcd_toolbox.h"
#include "gpio_toolbox.h"
#include "timer_toolbox.h"
#include "bits.h"
#include "applications.h"
#include "sd_toolbox.h"


void general_init();
void init_tests_sd();
void tests_sd();
/* Global variables */
vulong AppStack_svr[APPSTACKSIZE/4];
vulong AppStack_irq[APPSTACKSIZE/4];
int t[8];
//Tableaux pour les tests
//Lectures
vulong dataSimpleBlockR[128];
vulong dataMultipleBlockR1[128];
vulong dataMultipleBlockR2[256];
vulong dataMultipleBlockR8[1024];
//Ecritures
vulong dataSimpleBlockW[128];
vulong dataMultipleBlockW1[128];
vulong dataMultipleBlockW2[256];
vulong dataMultipleBlockW8[1024];

/* main */
int main(void)
{
	//On lance l'initialisation
	general_init();
	//On nettoie l'cran
	clear_screen();
	//On lance l'application
	//launchApp();

	//On lance les tests du labo sdcard1
	tests_sd();

	while(1);
    return(0);
}

//Fonction comprenant les diffrentes initialisations
void general_init()
{
	lcd_off();
	lcd_init();
	lcd_on();
	GPIO_init();
	interrupt_init();
	timer_init();
	mmc1_init();


}

void init_tests_sd()
{
	//Initialisation des tableaux d'écriture
	int i = 0;
	for(; i < 128; i++)
	{
		dataSimpleBlockW[i] = 0x11223344;
		dataMultipleBlockW1[i] = 0x44556677;
	}
	i = 0;
	for(;i < 256; i++)
	{
		dataMultipleBlockW2[i] = 0xCAFECAFE;
	}
	i = 0;
	for(;i < 1024; i++)
	{
		dataMultipleBlockW8[i] = 0xBEBEBEBE;
	}

	//Initialisation des tableaux de lectures
	i = 0;
	for(; i < 128; i++)
	{
		dataSimpleBlockR[i] = 0x0;
		dataMultipleBlockR1[i] = 0x0;
	}
	i = 0;
	for(;i < 256; i++)
	{
		dataMultipleBlockR2[i] = 0x0;
	}
	i = 0;
	for(;i < 1024; i++)
	{
		dataMultipleBlockR8[i] = 0x0;
	}

}

//Fonction permettant de tester les fonctionalitées implémentées. Pour la procedure de test exact, regarder le README
void tests_sd()
{
	//Affichage du nom du produit
	uchar temp[5];
	read_productname(temp);
	fb_print_string(temp,100,50,14);
	//Affichage de la taille de la carte
	ulong size = read_card_size();
	uchar messSize[80];
	sprintf(messSize,"Size(totale): %u KB",size);
	fb_print_string(messSize,100,100,14);

	//Initialisation des tableaux
	init_tests_sd();

	//Tests : Lecture blocs 0,1,2 et 3
	int result = mmchs_read_block(dataSimpleBlockR,0);
	result = mmchs_read_block(dataSimpleBlockR,1);
	result = mmchs_read_block(dataSimpleBlockR,2);
	result = mmchs_read_block(dataSimpleBlockR,3);
	//Fin Tests : Lecture blocs 0,1,2 et 3
	int test_breakpoint = 0;

	//Tests : Lecture multiple blocks 1,2 et 8
	result = mmchs_read_multiple_block(dataMultipleBlockR1,0,1);
	result = mmchs_read_multiple_block(dataMultipleBlockR2,0,2);
	result = mmchs_read_multiple_block(dataMultipleBlockR8,0,8);
	//Fin Tests : Lecture multiples blocks 1,2 et 8
	test_breakpoint = 0;

	//Tests : Ecriture block 4,5,6 et 7
	result = mmchs_write_block(dataSimpleBlockW,4);
	result = mmchs_write_block(dataSimpleBlockW,5);
	result = mmchs_write_block(dataSimpleBlockW,6);
	result = mmchs_write_block(dataSimpleBlockW,7);
	//Fin Tests : Ecriture block 4,5,6 et 7
	result = mmchs_read_multiple_block(dataMultipleBlockR8,4,4);
	test_breakpoint = 0;
	init_tests_sd();

	//Tests : Ecriture multiple blocks 1,2 et 8
	result = mmchs_write_multiple_block(dataMultipleBlockW1,4,1);
	result = mmchs_read_multiple_block(dataMultipleBlockR1,4,1);
	result = mmchs_write_multiple_block(dataMultipleBlockW2,4,2);
	result = mmchs_read_multiple_block(dataMultipleBlockR2,4,2);
	result = mmchs_write_multiple_block(dataMultipleBlockW8,4,8);
	result = mmchs_read_multiple_block(dataMultipleBlockR8,4,8);
	//Fin Tests : Ecriture multiple blocks 1,2 et 8
	test_breakpoint = 0;
}

