/* ----------------------------------------------------------------------
   HEIG-VD Yverdon-les-Bains
   ------------------------------------------------------------------------

   Auteur : Pierrick MULLER
   Email  : pierrick.muller@heig-vd.ch
   Date   : 21/03/2019
   Description : TIMER toolbox header
   -----------------------------------------------------------------------
*/

#ifndef TIMER_TOOLBOX_H_
#define TIMER_TOOLBOX_H_

#include "stddefs.h"

//Variable global stockant la valeur du timer
extern uint valTimer;


extern void start_timer();
extern void stop_timer();
extern uint read_timer_value();
extern void write_timer_value(uint value);
extern void sleep(uint sec);


#endif /* TIMER_TOOLBOX_H_ */
