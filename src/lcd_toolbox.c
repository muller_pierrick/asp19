/* ----------------------------------------------------------------------
   Institut ReDS - HEIG-VD Yverdon-les-Bains
   ------------------------------------------------------------------------

   Auteur : Evangelina LOLIVIER-EXLER
   Email  : evangelina.lolivier-exler@heig-vd.ch
   Date   : 17/09/2008
   Modified: 24/05/2012 REPTAR board adaptation
   Description : LCD toolbox
   -----------------------------------------------------------------------
*/

#include "stddefs.h"
#include "lcd.h"
#include "vga_lookup.h"
#include "fb_fonts.h"
#include "gpio.h"
#include "prcm.h"
#include "bits.h"

#define LINES_PER_SCREEN        (PIXELS_PER_COL/FONT_HEIGHT)


// globals to keep track of foreground, background colors and x,y position
uchar lcd_color_depth=16;       // 16 bits per pixel
uchar lcd_bg_color=3;           // 0 to 15, used as lookup into VGA color table
uchar lcd_fg_color=8;           // 0 to 15, used as lookup into VGA color table



//------------------------------------------------------------------------------
// Delay for some usecs. - Not accurate
// and no Cache
void udelay(int delay)
{
    volatile int i;
    for ( i = LOOPS_PER_USEC * delay; i ; i--)
        ;
}



///--------------------------------------------------------------------------
// lcd_on
//
// This function turns on the DM3730 Display Controller
//
void lcd_on()
{

    // Enable the clocks to the DISPC
    // enable functional clock (will be enable with the LCD_on function)
    DSS_CM_REG(CM_FCLKEN)|=BIT0;
    // enable interfaces clocks (L3 and L4)  (will be enable with the LCD_on function)
    DSS_CM_REG(CM_ICLKEN)|=BIT0;

    // power up the LCD
    udelay(10000);
    // LCD output enable
    LCD_REG(control)|= BIT0;
    udelay(10000);

}

//--------------------------------------------------------------------------
// lcd_off
//
// This function turns off the DM3730 Display Controller
//
void lcd_off()
{
    // power down the LCD
    udelay(10000);

    // digital output disable
    LCD_REG(control)&=~BIT1;
    // LCD output disable
    LCD_REG(control)&=~BIT0;
    udelay(10000);
}




// ------------------------------------------------------------------
// get_pixel_add return the address of a pixel with respect to the frame buffer start address
ulong get_pixel_add (ulong X, ulong Y)
{
	//On multiplie le nombre de ligne par y et on ajoute X. On fait fois deux car chaque pixel est cod� sur 2 octet
	return (ulong) (((Y*PIXELS_PER_ROW) + X)*2);
}

// ------------------------------------------------------------------
// fb_set_pixel sets a pixel to the specified color (between 0 et 15).
void fb_set_pixel(ulong X, ulong Y, uchar color)
{
	LCD_BUF(get_pixel_add(X,Y)) = color;
}



// ------------------------------------------------------------------
// fb_print_char prints a character at the specified location.
//
void fb_print_char(uchar cchar, ulong x, ulong y, uchar color)
{
	int i;
	int j;
	//On parcourt la hauteur de la police
	for(i = 0;i < FONT_HEIGHT; i++)
	{
		//On va chercher la valeur du caract�re correspondante a la ligne de la hauteur
		int val = fb_font_data[cchar-0x20][i];
		//On cr�e un masque correspondant au premier pixel de la ligne
		int masque = 0x80;
		//On affiche le pixel si le bit correspondant est � 1 et on d�calle le masque pour tester le prochain pixel.
		for(j = 0; j < FONT_WIDTH; j++)
		{
			if(val & masque)
			{
				fb_set_pixel(x+j,y+i,color);
			}
			masque = masque >> 1;
		}
	}
}

// ------------------------------------------------------------------
// fb_print_string prints a string at the specified location.(30 characters max)
//
void fb_print_string(uchar *pcbuffer, ulong x, ulong y, uchar color)
{
	int i = 0;
	ulong xLoc = x;
	//Tant qu'on est pas � la fin du string, on affiche chaque caract�re
	while(pcbuffer[i] != '\0' && i < 30)
	{
		fb_print_char(pcbuffer[i],xLoc,y,color);
		xLoc += FONT_WIDTH;
		i++;

	}
}

// ------------------------------------------------------------------
//
// This function clear the screen
//
void clear_screen()
{
	int i;
	//On met tous les pixels de l'�cran en blanc
	for(i = 0; i < PIXELS_PER_COL * PIXELS_PER_ROW ; i++)
	{
			//On utilise une macro, mais on doit faire fois 2 car chaque pixel est cod� sur deux octets
			LCD_BUF(i*2) =  LU_WHITE;
	}
}
