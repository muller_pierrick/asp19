/*
 * applications.c
 *
 *  Created on: Mar 28, 2019
 *  Author: Pierrick Muller
 */

//Variable contenant la valeur du timer

#include "lcd_toolbox.h"
#include "bits.h"
#include "gpio_toolbox.h"
#include "timer_toolbox.h"
#include <time.h>
#include <stdlib.h>

//Variables permettant de gerer l'affichage et la logique de l'application
uint valueTimer = 0;
uint score = 0;
uint bestScore = 0;
uchar running = 0;

void launchApp()
{

	valueTimer = 0;
	bestScore = -1;
	while(1)
	{
		//Affichage du menu
		printMenu();
		//Reset de la valeur stock�e du timer
		valueTimer = 0;
		//D�marage du gestionaire d'allumage de led
		startLed();
		//Attente de l'interruption (SW0)
		while(valueTimer == 0);
		//On pr�cise que l'interruption ne doit plus effectuer sa logique
		running = 0;
		//On �teind la led
		ToggleOutput(5,BIT15);
	}

}

void startLed()
{
	//Variable representant le temps d'attente de l'allumage de la led
	uint time = 0;

	//Boucle attendant l'appui sur le bouton start
	while(1)
	{
		//Gestion de l'appui sur SW1 par pulling
		if(ReadInput(5,BIT14) == (BIT14 >> 8))
		{
			//On pr�cise sur l'�cran que le logiciel tourne
			fb_print_string("Running...",20,300,14);
			//On calcule le temps al�atoire et on attend durant ce temps
			time = rand() % 10;
			sleep(time);
			//cette variable sert a activer le fonctionnement de l'interruption
			running = 1;
			//On allume la led et on start le timer
			ToggleOutput(5,BIT15);
			start_timer();
			break;
		}

	}
}

void checkBestScore()
{
	if(score < bestScore)
	{
		bestScore = score;
	}
}

void printMenu()
{
	//On met a jour l'affichage
	clear_screen();

	//AFFICHAGE TITRE
	fb_print_string("BIENVENU SUR LE JEU",300,10,14);
	fb_print_string("---------------------",290,20,14);

	//AFFICHAGE TEXTE DEBUT
	fb_print_string("Ce jeu mesure votre temps de",20,50,14);
	fb_print_string("reaction.Quand vous serez pret,",250,50,14);
	fb_print_string(",appuyer sur le bouton start",490,50,14);
	fb_print_string("(SW1) et observez la led LED0.",20,70,14);
	fb_print_string("Une fois cette led allumee, ",260,70,14);
	fb_print_string("appuyez le plus rapidement ",480,70,14);
	fb_print_string("possible sur le bouton SW0.",20,90,14);
	fb_print_string("Bonne chance !",20,110,14);

	//AFFICHAGE COMMANDES
	fb_print_string("COMMANDES :",20,140,14);
	fb_print_string("Start - SW1",20,160,14);
	fb_print_string("Stop - SW0",20,180,14);


	//AFFICHAGE RESULT
	//Variables pour affichage
	uchar scoreText[80];
	uchar bestScoreText[80];

	//On calcule le temps en ms
	score = valueTimer/32;

	//On affiche un resultat different si c'est le premier lancement du jeu ou pas
	if(score != 0)
	{
		checkBestScore();
		sprintf(scoreText,"Score en ms : %u",score);
		sprintf(bestScoreText,"Meilleur Score en ms : %u",bestScore);

	}
	else
	{
		strcpy(scoreText,"Score en ms : -");
		strcpy(bestScoreText,"Meilleur Score en ms : -");
	}
	fb_print_string(scoreText,500,300,14);
	fb_print_string(bestScoreText,500,320,14);

}
